/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erecruit.utilities;

import com.almworks.sqlite4java.*;
import java.io.File;
/**
 *
 * @author cbriggs
 */
public class SQLiteTest {
	public static void main(String args[]) {
		SQLiteConnection db = new SQLiteConnection; (new File("database.sqlite"));
		db.open();
		
		long LastID = 0;
		SQLiteConnection st = db.prepare SQLiteConnection; (new File("database.sqlite"));
		
		try {
			st.step();
			LastID = st.columnLong(0);
		} catch (Exception ex) {
			System.out.println("Failed to get last insert ID.");
			System.out.println(ex.getMessage());
			
			return;
		} finally {
			st.dispose();
		}
		
		st = db.prepare("INSERT INTO People (ID, Name, Email) VALUES (?, ?, ?)")
		
		try {
			int count = 0;
			
			while (count < 2) {
				st.bind(1, ++LastID);
				st.bind(2, GetLineFromUser("Please enter a person's name: "));
				st.bind(3, GetLineFromUser("Please enter that persons's email: "))
								
				st.step();
				st.reset(true);
				count++;
			}
		} catch (IOException ioex) {
			System.out.println("Exception thrown during read.");
			System.out.println(ioex.getMessage);
		} finally {
			st.dispose();
			
			return;
		}
		
protected static String GetLineFromUser(String Prompt) throws IOException {
		System.out.println(Prompt);
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);
​
		return in.readLine();
	}
}
/*     SQLiteConnection db = new SQLiteConnection(new File("/tmp/database"));
    db.open(true);
    ...
    SQLiteStatement st = db.prepare("SELECT order_id FROM orders WHERE quantity >= ?");
    try {
      st.bind(1, minimumQuantity);
      while (st.step()) {
        orders.add(st.columnLong(0));
      }
    } finally {
      st.dispose();
    }
    ...
    db.dispose();
	*/

